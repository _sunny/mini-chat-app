import React from "react";
import Messenger from "../Messenger";
import { Redirect, Switch, Route } from "react-router-dom";
import { shallowEqual, useSelector } from "react-redux";
import Login from "../auth/Login";
import Logout from "../auth/Logout";
export default function App() {
  const { isAuthorized } = useSelector(
    ({ auth }) => ({
      isAuthorized: auth.user != null
    }),
    shallowEqual
  );
  return (
    <div className="App">
      <Switch>
        {!isAuthorized ? (
          /*Render auth page when user at `/auth` and not authorized.*/
          <Route path="/login" component={Login} />
        ) : (
          /*Otherwise redirect to root page (`/`)*/
          <Redirect from="/login" to="/message" />
        )}
        <Route path="/logout" component={Logout} />

        {!isAuthorized ? <Redirect to="/login" /> : <Route exact path="/message" component={Messenger} />}
      </Switch>
    </div>
  );
}

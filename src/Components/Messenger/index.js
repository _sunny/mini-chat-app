import React, { useEffect, useState, useRef } from "react";
import { useFormik } from "formik";
import io from "socket.io-client";
import { SOCKET_URL, BASE_URL } from "../../Store/config";
import axios from "axios";

export default function Messenger(props) {
  const socket = useRef(null);
  const roomID = 1;
  const getInputClasses = fieldname => {
    if (formik.touched[fieldname] && formik.errors[fieldname]) {
      return "is-invalid";
    }

    if (formik.touched[fieldname] && !formik.errors[fieldname]) {
      return "is-valid";
    }

    return "";
  };

  useEffect(() => {
    socket.current = io(`${SOCKET_URL}`);

    socket.current.on("connect", function () {
      socket.current.emit("addToRoom", roomID);
    });

    socket.current.on("EconsSecretChat", res => {
      console.log(res);
    });

    return () => socket.current.disconnect();
  }, []);
  const initialValues = {
    body: ""
  };
  const formik = useFormik({
    initialValues,
    onSubmit: (values, { setStatus, setSubmitting }) => {
      axios.post(`${BASE_URL}/chat/sendMessage`, {
        roomId: roomID,
        body: values.body
      });
    }
  });
  return (
    <div className="container messenger">
      <form onSubmit={formik.handleSubmit}>
        <div className="form-group fv-plugins-icon-container">
          <label>Message</label>
          <input placeholder="Message" type="text" className={`form-control form-control-solid h-auto  ${getInputClasses("body")}`} name="body" {...formik.getFieldProps("body")} />
        </div>

        <button type="submit" className="btn btn-dark btn-lg btn-block">
          Send message
        </button>
      </form>
    </div>
  );
}

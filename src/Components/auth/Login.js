import React from "react";
import { useFormik } from "formik";
import * as auth from "../../Store/reducers/auth/authRedux";
import { login } from "../../Store/reducers/auth/authCrud";
import { connect } from "react-redux";
function Login(props) {
  const getInputClasses = fieldname => {
    if (formik.touched[fieldname] && formik.errors[fieldname]) {
      return "is-invalid";
    }

    if (formik.touched[fieldname] && !formik.errors[fieldname]) {
      return "is-valid";
    }

    return "";
  };
  const initialValues = {
    email: "",
    password: "",
    userType: "docStaff"
  };
  const formik = useFormik({
    initialValues,
    onSubmit: (values, { setStatus, setSubmitting }) => {
      setTimeout(async () => {
        const getToken = "123123123";

        login(values.email, values.password, values.userType, getToken)
          .then(res => {
            props.login(res && res.data.data.token);
          })
          .catch(() => {
            setSubmitting(false);
          });
      }, 1000);
    }
  });
  return (
    <div className="container">
      <form onSubmit={formik.handleSubmit}>
        <h3>Log in</h3>

        <div className="form-group fv-plugins-icon-container">
          <label>Email</label>
          <input placeholder="Email" type="email" className={`form-control form-control-solid h-auto ${getInputClasses("email")}`} name="email" {...formik.getFieldProps("email")} />
        </div>

        <div className="form-group fv-plugins-icon-container">
          <label>Password</label>
          <input placeholder="Password" type="password" className={`form-control form-control-solid h-auto  ${getInputClasses("password")}`} name="password" {...formik.getFieldProps("password")} />
        </div>

        <button type="submit" className="btn btn-dark btn-lg btn-block">
          Sign in
        </button>
      </form>
    </div>
  );
}
export default connect(null, auth.actions)(Login);

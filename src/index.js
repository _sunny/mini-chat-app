import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import App from "./Components/App";
import "./App.scss";
import axios from "axios";
import Store, { persistor } from "./Store";
import setupAxios from "./Store/setupAxios";
import reportWebVitals from "./reportWebVitals";
const { PUBLIC_URL } = process.env;
setupAxios(axios, Store);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={Store}>
      <PersistGate persistor={persistor}>
        <BrowserRouter>
          <App basename={PUBLIC_URL} />
        </BrowserRouter>
      </PersistGate>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();

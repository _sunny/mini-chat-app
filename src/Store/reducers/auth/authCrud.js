import axios from "axios";
import { BASE_URL } from "../../config";
export const LOGIN_URL = `${BASE_URL}/login`;
export const REGISTER_URL = `${BASE_URL}/api/auth/register`;
export const REQUEST_PASSWORD_URL = `${BASE_URL}/forgotPassword`;
export const RESET_PASSWORD_URL = `${BASE_URL}/resetPassword`;
export const PATIENT_REGISTER_URL = `${BASE_URL}/patient/register`;
export const INDEPENDENT_DOCTOR_REGISTER_URL = `${BASE_URL}/docStaff/indregister`;
export const CREATE_STAF_URKL = `${BASE_URL}/docStaff/staffregister`;
export const CLINIC_REGISTER_URL = `${BASE_URL}/clinic/clinicRegister`;
export const ADD_STAFF_IN_REGISTRATION = `${BASE_URL}/clinic/addStaffWithNoToken`;
export const ME_URL = `${BASE_URL}/me/me`;

export function login(email, password, userType, token) {
  const headersLogin = {
    type: `${userType}`
  };

  return axios.post(LOGIN_URL, { email, password, fcmToken: token }, { headers: headersLogin });
}

export function register(email, fullname, username, password) {
  return axios.post(REGISTER_URL, { email, fullname, username, password });
}

export function requestPassword(email, userType) {
  const headersType = {
    type: `${userType}`
  };

  return axios.post(REQUEST_PASSWORD_URL, { email }, { headers: headersType });
}

export function resetPassword(email, code, password, confirmPassword, userType) {
  const headersType = {
    type: `${userType}`
  };

  return axios.post(RESET_PASSWORD_URL, { email, code, password, confirmPassword }, { headers: headersType });
}

export function createPatient(fName, lName, email, gender, birthDate, insurance) {
  const headersType = {
    type: `patient`
  };

  return axios.post(PATIENT_REGISTER_URL, { fName, lName, email, gender, birthDate, insurance }, { headers: headersType });
}
export function createClinic(data) {
  const headersType = {
    type: `clinic`
  };

  return axios.post(CLINIC_REGISTER_URL, { ...data }, { headers: headersType });
}

export function addStaffRegistration(data) {
  return axios.post(ADD_STAFF_IN_REGISTRATION, { ...data });
}

export function createIndependentDoctor(data) {
  const headersType = {
    type: `docStaff`
  };

  return axios.post(INDEPENDENT_DOCTOR_REGISTER_URL, { ...data }, { headers: headersType });
}
export function checkNPI(id) {
  return axios.post("https://econsultz.com/v1/api/getNPI", { npi: id });
}

export function createStaff(data) {
  return axios.post(CREATE_STAF_URKL, { ...data });
}

export function getUserByToken() {
  // Authorization head should be fulfilled in interceptor.
  return axios.post(ME_URL);
}

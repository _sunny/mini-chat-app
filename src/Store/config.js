const BASE_URL = "https://econsultz.com/v1/api";
const IMAGE_URL = "https://econsultz.com";
const SOCKET_URL = "https://econsultz.com";
export { BASE_URL, IMAGE_URL, SOCKET_URL };
